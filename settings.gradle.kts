enableFeaturePreview("GRADLE_METADATA")
rootProject.name = "config"

pluginManagement {
  resolutionStrategy {
    eachPlugin {
      if (requested.id.id.startsWith("org.jetbrains.dokka")) {
        // Update Version Build Source if being changed.
        useVersion("1.4.20")
      }
      if (requested.id.id.startsWith("org.jetbrains.kotlin.") || requested.id.id.startsWith("org.jetbrains.kotlin.plugin.serialization")) {
        // Update Version Build Source if being changed.
        useVersion("1.4.32")
      }
      if (
        requested.id.id.startsWith("design.animus.kotlin.mp.schemas.json.generator")
      ) {
        useVersion("0.0.1-SNAPSHOT")
      }
      if (
        requested.id.id.startsWith("org.gradle.kotlin.kotlin-dsl")
      ) {
        useVersion("2.0.0")
      }
    }
  }
  repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
    gradlePluginPortal()
    maven { url = java.net.URI("https://dl.bintray.com/kotlin/kotlinx") }
    maven { url = java.net.URI("https://kotlin.bintray.com/kotlin-dev") }
    maven { url = uri("https://git.local.animus.design/api/v4/groups/37/-/packages/maven") }
  }
}
