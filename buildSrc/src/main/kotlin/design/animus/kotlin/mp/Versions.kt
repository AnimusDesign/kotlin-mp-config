package design.animus.kotlin.mp

object Versions {
    object Plugins {
        const val kotlin = Dependencies.kotlin
        const val gradlePluginPublish = "0.10.1"
    }

    object Dependencies {
        const val kotlin = "1.4.32"
        const val serialization = "1.1.0"
        const val coroutines = "1.4.3"
        const val junit = "4.12"

    }
}
