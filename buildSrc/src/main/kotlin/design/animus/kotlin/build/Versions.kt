package design.animus.kotlin.build

object Versions {
    object Dependencies {
        const val kotlin = "1.4.30"
        const val serialization = "1.0.1"
        const val coroutine = "1.4.2"
        const val junit = "4.12"
    }
}
