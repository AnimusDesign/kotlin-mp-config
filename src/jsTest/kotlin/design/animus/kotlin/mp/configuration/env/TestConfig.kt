package design.animus.kotlin.mp.configuration.env

object TestConfig {
  val fruit: String by Environment("FRUIT")
}
