package design.animus.kotlin.mp.configuration.env

import kotlin.reflect.KProperty

internal actual fun <T : Any?> getValueFromEnvVar(varName: String?, thisRef: Any?, prop: KProperty<*>): T {
  val raw = if (varName != null) {
    process.env[varName]
  } else {
    process.env[prop.name]
  }
  return raw.unsafeCast<T>()
}
