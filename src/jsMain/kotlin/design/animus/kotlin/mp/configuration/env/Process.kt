package design.animus.kotlin.mp.configuration.env

external val process: Process

external interface Process {
  val env: dynamic
}
