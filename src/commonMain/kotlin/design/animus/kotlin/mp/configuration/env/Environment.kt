package design.animus.kotlin.mp.configuration.env

import kotlin.reflect.KProperty

class EnvironmentVariableNotFound(private val error: String) : Exception(error)

class Environment<T : Any?>(val varName: String? = null) {

  operator fun getValue(thisRef: Any?, prop: KProperty<*>): T {
    return getValueFromEnvVar<T>(varName, thisRef, prop)
  }
}

internal expect fun <T : Any?> getValueFromEnvVar(varName: String?, thisRef: Any?, prop: KProperty<*>): T
