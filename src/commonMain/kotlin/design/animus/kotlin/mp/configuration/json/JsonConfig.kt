@file:Suppress("EXPERIMENTAL_API_USAGE")

package design.animus.kotlin.mp.configuration.json
//
// import kotlinx.serialization.KSerializer
// import kotlinx.serialization.Serializable
// import kotlinx.serialization.json.Json
// import kotlinx.serialization.parse
//
// interface IJsonConfig
//
// expect suspend fun <C : IJsonConfig> IJsonConfig.loadJsonConfigFromFile(path: String, serializer: KSerializer<C>): C
