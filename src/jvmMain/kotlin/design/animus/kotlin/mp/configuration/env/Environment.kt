package design.animus.kotlin.mp.configuration.env

import java.math.BigDecimal
import java.math.BigInteger
import kotlin.reflect.KProperty

private fun stringResponse(): String = TODO()
private fun intResponse(): Int = TODO()
private fun floatResponse(): Float = TODO()
private fun doubleResponse(): Double = TODO()
private fun bigDecimalResponse(): BigDecimal = TODO()
private fun longResponse(): Long = TODO()
private fun bigIntegerResponse(): BigInteger = TODO()

@Suppress("UNCHECKED_CAST")
internal actual fun <T : Any?> getValueFromEnvVar(varName: String?, thisRef: Any?, prop: KProperty<*>): T {
  val raw = (if (varName == null) System.getenv(prop.name) else System.getenv(varName))
    ?: throw EnvironmentVariableNotFound("Environment variable $varName / ${prop.name} could not be found.")
  return when (prop.returnType) {
    ::stringResponse.returnType -> raw
    ::intResponse.returnType -> raw.toInt()
    ::floatResponse.returnType -> raw.toFloat()
    ::doubleResponse.returnType -> raw.toDouble()
    ::bigDecimalResponse.returnType -> raw.toBigDecimal()
    ::longResponse.returnType -> raw.toLong()
    ::bigIntegerResponse.returnType -> raw.toBigInteger()
    else -> raw
  } as T
}
