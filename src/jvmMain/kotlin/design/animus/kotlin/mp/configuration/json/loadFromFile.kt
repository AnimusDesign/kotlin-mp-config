@file:Suppress("EXPERIMENTAL_API_USAGE")

package design.animus.kotlin.mp.configuration.json

// import kotlinx.coroutines.runBlocking
// import kotlinx.serialization.KSerializer
// import kotlinx.serialization.Serializable
// import kotlinx.serialization.json.Json
// import java.io.File
//
//
// @Serializable
// data class TestJsonConfig(
//    val url: String
// ) : IJsonConfig
//
// actual suspend fun <C : IJsonConfig> IJsonConfig.loadJsonConfigFromFile(
//    path: String,
//    serializer: KSerializer<C>
// ): C {
//    val rawData = File(path).readText()
//    return parse(rawData, serializer)
// }
//
// suspend fun <C:IJsonConfig> C.makeSomething(
//    path: String,
//    serializer: KSerializer<C>
// ): C {
//    val rawData = File(path).readText()
//    println(rawData)
//    val x= Json.parse(serializer, rawData)
//    return x
//
// }
//
// fun main() = runBlocking {
//    val serializer = TestJsonConfig.serializer()
//    val a = TestJsonConfig.makeSomething("/tmp/config.json", serializer)
//    println(a)
// }
//
