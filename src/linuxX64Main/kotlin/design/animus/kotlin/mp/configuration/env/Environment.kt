package design.animus.kotlin.mp.configuration.env

import kotlin.reflect.KProperty
import kotlinx.cinterop.toKString
import platform.posix.getenv

private fun stringResponse(): String = TODO()
private fun intResponse(): Int = TODO()
private fun floatResponse(): Float = TODO()
private fun doubleResponse(): Double = TODO()
private fun longResponse(): Long = TODO()

@Suppress("UNCHECKED_CAST")
internal actual fun <T : Any?> getValueFromEnvVar(varName: String?, thisRef: Any?, prop: KProperty<*>): T {
  val raw = (if (varName == null) getenv(prop.name) else getenv(varName))
    ?.toKString()
    ?: throw EnvironmentVariableNotFound("Environment variable $varName / ${prop.name} could not be found.")
  return when (prop.returnType) {
    ::stringResponse.returnType -> raw
    ::intResponse.returnType -> raw.toInt()
    ::floatResponse.returnType -> raw.toFloat()
    ::doubleResponse.returnType -> raw.toDouble()
    ::longResponse.returnType -> raw.toLong()
    else -> raw
  } as T
}
