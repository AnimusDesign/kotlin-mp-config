package design.animus.kotlin.mp.configuration.env

import org.junit.Test
import kotlin.test.assertTrue

class EnvTest {
  @Test
  fun readTestVariable() {
    println(TestConfig.testVariable)
    assertTrue { TestConfig.testVariable == "hello" }
  }

  @Test
  fun readCamelTest() {
    println(TestConfig.camelCaseVariable)
    assertTrue { TestConfig.camelCaseVariable == 42 }
  }
}
