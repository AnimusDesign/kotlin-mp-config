package design.animus.kotlin.mp.configuration.env

object TestConfig {
  val testVariable: String by Environment("TESTVARIABLE")
  val camelCaseVariable: Int by Environment()
}
