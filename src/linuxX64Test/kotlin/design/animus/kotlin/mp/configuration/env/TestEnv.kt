package design.animus.kotlin.mp.configuration.env

import kotlin.test.Test
import kotlin.test.assertTrue

class TestEnv {
  @Test
  fun testEnv() {
    println(TestConfig)
    assertTrue { TestConfig.fruit == "banana" }
  }
}
