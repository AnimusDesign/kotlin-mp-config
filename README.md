# Kotlin Mp Config

A multiplatform config creator from environment variables using 
delegates. It is typesafe as a basic type. Casting the env var
to the desired type or throwing an error..

```kotlin
object TestConfig {
    val testVariable: String by Environment("TESTVARIABLE")
    val camelCaseVariable: Int by Environment()
    val magicalBananaMan: String by Environment()
}
```
