import design.animus.kotlin.mp.Versions.Dependencies
import java.net.URI
plugins {
  kotlin("multiplatform")
  id("org.jetbrains.dokka")
  id("maven-publish")
  kotlin("plugin.serialization")
  id("org.jlleitschuh.gradle.ktlint") version "10.0.0"
  id("net.researchgate.release") version "2.8.1"
}

group = "design.animus.kotlin.mp"

repositories {
  maven { url = URI("https://animusdesign-repository.appspot.com") }
  mavenCentral()
  jcenter()
  google()
  maven { url = URI("https://kotlin.bintray.com/kotlinx") }
  maven { url = URI("https://dl.bintray.com/kotlin/kotlin-js-wrappers") }
  maven { url = URI("https://kotlin.bintray.com/kotlinx") }
  maven { url = URI("https://maven.google.com") }
  maven { url = URI("https://plugins.gradle.org/m2/") }
  maven { url = URI("http://dl.bintray.com/kotlin/kotlin-dev") }
  maven { url = URI("https://dl.bintray.com/kotlin/kotlin-eap") }
  mavenLocal()
}

kotlin {
  metadata { }
  jvm {
    compilations.named("main") {
      // kotlin compiler compatibility options
      kotlinOptions {
        apiVersion = "1.4"
        languageVersion = "1.4"
        jvmTarget = "11"
      }
    }
  }
  js {
    compilations.named("main") {
      kotlinOptions {
        metaInfo = true
        sourceMap = true
        verbose = true
        moduleKind = "umd"
      }
    }
    nodejs()
    binaries.executable()
  }
  val hostOs = System.getProperty("os.name")
  val isMingwX64 = hostOs.startsWith("Windows")
  val nativeTarget = when {
    hostOs == "Mac OS X" -> {
      iosX64 {}
      iosArm64 {}
      iosArm32 {}
      tvos {}
      tvosArm64 {}
      macosX64 {}
    }
    hostOs == "Linux" -> linuxX64 {}
    else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
  }

  sourceSets {
    val commonMain by getting {
      dependencies {
        api(kotlin("stdlib-common"))
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutines}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${Dependencies.serialization}")
      }
    }
    val commonTest by getting {
      dependencies {
        implementation(kotlin("test-common"))
        implementation(kotlin("test-annotations-common"))
      }
    }
    val jvmMain by getting {
      dependencies {
        api(kotlin("stdlib"))
        api(kotlin("reflect"))
      }
    }
    val jvmTest by getting {
      dependencies {
        implementation(kotlin("test"))
        implementation(kotlin("test-junit"))
        implementation("junit:junit:${Dependencies.junit}")
      }
    }
    val jsMain by getting {
      dependencies {
        api(kotlin("stdlib-js"))
        implementation("org.jetbrains.kotlinx:kotlinx-nodejs:0.0.7")
      }
    }
    val jsTest by getting {
      dependencies {
        implementation(kotlin("test-js"))
      }
    }
    val nativeMain by creating {
      dependsOn(commonMain)
    }
    val nativeTest by creating {
      dependsOn(commonTest)
    }
    when (nativeTarget.name) {
      "macosX64" -> {
        val iOSBase by creating {
          dependsOn(nativeMain)
        }
        val iosX64Main by getting {
          dependsOn(iOSBase)
        }
        val iosArm64Main by getting {
          dependsOn(iOSBase)
        }
        val iosArm32Main by getting {
          dependsOn(iOSBase)
        }
        val tvosMain by getting {
          dependsOn(iOSBase)
        }
        val tvosArm64Main by getting {
          dependsOn(iOSBase)
        }
        val macosX64Main by getting {
          dependsOn(nativeMain)
        }
      }
      "linuxX64" -> {
        val linuxX64Main by getting {
          dependsOn(nativeMain)
        }
        val linuxX64Test by getting {
          dependsOn(nativeTest)
        }
      }
    }
  }

  val proj = project
  tasks {
    val jvmTest by getting(org.jetbrains.kotlin.gradle.targets.jvm.tasks.KotlinJvmTest::class) {
      this.useJUnit()
      this.reports.junitXml.isEnabled = true
    }
  }

  apply(plugin = "org.jlleitschuh.gradle.ktlint")
  configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
    debug.set(true)
    verbose.set(true)
    android.set(false)
    enableExperimentalRules.set(true)
    additionalEditorconfigFile.set(file("$rootDir/.editorconfig"))
    filter {
      include("**/main/**", "**/src/main/**", "**/test/**", "**/src/test/**")
      exclude("**/generated/**", "generated")
    }
  }
  configure<PublishingExtension> {
    repositories {
      maven {
        url = URI("https://gitlab.com/api/v4/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven/")
        credentials(HttpHeaderCredentials::class.java) {
          name = "Job-Token"
          value = System.getenv("CI_JOB_TOKEN")
        }
        authentication {
          create<HttpHeaderAuthentication>("header")
        }
      }
    }
  }
}
